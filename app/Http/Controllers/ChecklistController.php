<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseFormatter;
use App\Models\Checklist;
use Illuminate\Http\Request;

class ChecklistController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:sanctum');
	}
	
    public function index()
	{
		$checklists = Checklist::all();
		
		return ResponseFormatter::success(
            [
                'checklists' => $checklists
            ],
            'Checklist berhasil ditampilkan'
        );
	}
	
	public function store(Request $request)
	{
		$checklist = new Checklist();
		$checklist->name = $request->name;
		$checklist->save();
		
		return ResponseFormatter::success(
            [
                'checklist' => $checklist
            ],
            'Checklist berhasil dibuat'
        );
	}
	
	public function destroy(Checklist $checklist)
	{
		$checklist->delete();
		
		return ResponseFormatter::success([], 'Checklist berhasil dihapus');
	}
}
