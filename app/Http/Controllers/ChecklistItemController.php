<?php

namespace App\Http\Controllers;

use App\Models\Checklist;
use Illuminate\Http\Request;
use App\Models\ChecklistItem;
use App\Helpers\ResponseFormatter;

class ChecklistItemController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:sanctum');
	}
	
    public function index(Checklist $checklist)
    {
        $checklistItems = ChecklistItem::where('checklist_id', $checklist->id)->get();
		
		return ResponseFormatter::success(
            [
                'checklistItems' => $checklistItems
            ],
            'Checklist Item berhasil ditampilkan'
        );
    }

    public function store(Request $request, Checklist $checklist)
    {
        $checklistItem = new ChecklistItem();
		$checklistItem->checklist_id = $checklist->id;
		$checklistItem->itemName = $request->itemName;
		$checklistItem->save();
		
		return ResponseFormatter::success(
            [
                'checklistItem' => $checklistItem
            ],
            'Checklist Item berhasil dibuat'
        );
    }

    public function show(Checklist $checklist, ChecklistItem $checklistItem)
    {
        return ResponseFormatter::success(
            [
                'checklistItem' => $checklistItem
            ],
            'Checklist Item Detail berhasil ditampilkan'
        );
    }

    public function changeStatus(Checklist $checklist, ChecklistItem $checklistItem)
    {
        $checklistItem->status = !$checklistItem->status;
		$checklistItem->save();
		
		return ResponseFormatter::success(
            [
                'checklistItem' => $checklistItem
            ],
            'Checklist Item Status berhasil diubah'
        );
    }

    public function destroy(Checklist $checklist, ChecklistItem $checklistItem)
    {
        $checklistItem->delete();
		
		return ResponseFormatter::success([], 'Checklist Item berhasil dihapus');
    }
	
    public function rename(Request $request, Checklist $checklist, ChecklistItem $checklistItem)
    {
        $checklistItem->itemName = $request->itemName;
		$checklistItem->save();
		
		return ResponseFormatter::success(
            [
                'checklistItem' => $checklistItem
            ],
            'Checklist Item Name berhasil diubah'
        );
    }
}
