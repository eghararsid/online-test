<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Helpers\ResponseFormatter;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;

class UserController extends Controller
{
    public function register(UserRegisterRequest $request)
    {
        $user = User::create([
            'password' => Hash::make($request->password),
            'username' => $request->username,
            'email' => $request->email,
        ]);
        
        $tokenResult = $user->createToken('authToken')->plainTextToken;
        
        return ResponseFormatter::success(
            [
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'user' => $user
            ],
            'User berhasil didaftarkan'
        );
    }
    
    public function login(UserLoginRequest $request)
    {
        $credentials = request(['username', 'password']);
        if (!auth()->attempt($credentials)) {
            return ResponseFormatter::error(
                [
                    'message' => 'Unauthorized'
                ],
                'Username atau password salah',
                500
            );
        }
        
        $user = User::where('username', $request->username)->first();
        $tokenResult = $user->createToken('authToken')->plainTextToken;
        
        return ResponseFormatter::success(
            [
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'user' => auth()->user()
            ],
            'User berhasil login'
        );
    }
}
