<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Checklist extends Model
{
    use HasFactory;
	
	protected $guarded = [];
	
	public function checklistItems()
	{
		return $this->hasMany(ChecklistItem::class);
	}
}
