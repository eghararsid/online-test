<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ChecklistController;
use App\Http\Controllers\ChecklistItemController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', [UserController::class, 'register'])->name('api.auth.register');
Route::post('/login', [UserController::class, 'login'])->name('api.auth.login');

route::group(['prefix' => 'checklist', 'name' => 'api.checklist.'], function () {
	Route::get('/', [ChecklistController::class, 'index'])->name('index');
	Route::post('/', [ChecklistController::class, 'store'])->name('store');
	Route::delete('/{checklist}', [ChecklistController::class, 'destroy'])->name('destroy');
});

route::group(['prefix' => 'checklist', 'name' => 'api.checklist.item.'], function () {
	Route::get('/{checklist}/item', [ChecklistItemController::class, 'index'])->name('index');
	Route::post('/{checklist}/item', [ChecklistItemController::class, 'store'])->name('store');
	Route::get('/{checklist}/item/{checklistItem}', [ChecklistItemController::class, 'show'])->name('show');
	Route::put('/{checklist}/item/{checklistItem}', [ChecklistItemController::class, 'changeStatus'])->name('status');
	Route::delete('/{checklist}/item/{checklistItem}', [ChecklistItemController::class, 'destroy'])->name('destroy');
	Route::put('/{checklist}/item/rename/{checklistItem}', [ChecklistItemController::class, 'rename'])->name('rename');
});

